# Hello Touch Designer

### Great getting-started resources

- https://www.youtube.com/watch?v=wmM1lCWtn6o
- https://matthewragan.com/introduction-to-touchdesigner-ldi-2018-touchdesigner/
- https://www.youtube.com/channel/UC_5e7-oykNV2CgtyS9LJ59w
- https://matthewragan.com/teaching-resources/touchdesigner/

# The Network Editor

- Navigating the heirarchy
- Using the address bar
- Using the scroll wheel and keyboard shortcuts (`I` to go IN, `U` to go UP)
- `H` to "home" the network
- You can resize nodes, but it'll get messy fast. So don't.
- Hitting the star in the bottom right makes the node interactive.

# Data Flow

- Data flows from left to right
- Wires will animate if data is flowing
- Space bar pauses and plays the timeline
- Using the paramaters window
- You can change values by click and drag or direct input
- All parameters can be Python expressions or controlled by input
- Every parameter window has its own help button

# Operators

- TOPs - Texture Operators - Image textures
- SOPs - Surface Operators - 3D geometry
- MATs - Material Operators - Materials for 3D objects
- CHOPs - Channel Operators - Channels of data
- DATs - Data Operators - Text, tables, code, data sources
- COMPs - Component Operators - Component tools
- Create operators by hitting TAB or double-clicking the network background
- You can start typing to filter the operator list
- Connect nodes by dragging outputs to inputs or right-clicking outputs
- Right click nodes to get information about the node
- Every node has Bypass and Lock options
- You can name them whatever you want!
- You can convert between operator types
- Use NULL operators as an organizational tool
